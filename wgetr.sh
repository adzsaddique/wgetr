#!/bin/bash 

echo "Starting wGetr"

startdl() {
	linkfile=${1:-links.txt}
	nohup wget -c -i $linkfile &
}


watchdl(){
	if [ ! -z $1 ]
	then 
		watch -n 0.5 "grep -i 'Saving to' nohup.out | tail -n 1 && tail -n 3 nohup.out && echo "" && ls -1hrt | grep $1"
	else
		watch -n 0.5 "grep -i 'Saving to' nohup.out | tail -n 1 && tail -n 3 nohup.out && echo "" && ls -1hrt"
	fi
}


echo "Done"
