# README #

wGetr is a rudimentary download utility which uses wGet's -i option to read a list of links to download from and can read report the status of the downloads in the terminal window

### How do I get set up? ###

1. Save the wgetr script somewhere static
2. Run this line or add to .bashrc to always run it on login
 
```
#!shell
source /path/to/script.sh
```

### How do I use wGetr? ###

wGetr defines two functions: `startdl` and `watchdl`

First, create a text file within your target folder containing the links you want to download. By default, startdl expects the file to be called `links.txt`, but you can override this through a parameter. 

Once you have the file, run the command `startdl` or `startdl myfilename.txt` if you named it something other than links.txt. 

**Optional** You can run `watchdl` to keep a track of the download progress on screen. If your folder contains unrelated files, `watchdl` supports a parameter to filter the folder contents list using grep. `watchdl mkv` or `watchdl exe` will only show the files within the directory which contain mkv or exe respectively. 
NB: This only affects the directory listing and not the download in any way. 